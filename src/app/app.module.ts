import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MainComponent } from './components/main/main.component';
import { OthersListComponent } from './components/others-list/others-list.component';
import { PokemonFigureComponent } from './components/pokemon-figure/pokemon-figure.component';
import { PkmDetailsComponent } from './components/pkm-details/pkm-details.component';
import { PkmImgComponent } from './components/pkm-img/pkm-img.component';
import { PkmTitleComponent } from './components/pkm-title/pkm-title.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    OthersListComponent,
    PokemonFigureComponent,
    PkmDetailsComponent,
    PkmImgComponent,
    PkmTitleComponent,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
