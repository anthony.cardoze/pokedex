import { Component, Input } from '@angular/core';

import OTHERS from '../../service/OTHERS';

@Component({
  selector: 'app-others-list',
  templateUrl: './others-list.component.html',
  styleUrls: ['./others-list.component.css'],
})
export class OthersListComponent {
  @Input() OTHERS: {
    imageSrc: string;
    imageAlt: string;
  }[] = OTHERS;
}
