import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PkmImgComponent } from './pkm-img.component';

describe('PkmImgComponent', () => {
  let component: PkmImgComponent;
  let fixture: ComponentFixture<PkmImgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PkmImgComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PkmImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
