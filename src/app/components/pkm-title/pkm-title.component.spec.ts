import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PkmTitleComponent } from './pkm-title.component';

describe('PkmTitleComponent', () => {
  let component: PkmTitleComponent;
  let fixture: ComponentFixture<PkmTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PkmTitleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PkmTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
