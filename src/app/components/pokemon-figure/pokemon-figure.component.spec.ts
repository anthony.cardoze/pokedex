import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonFigureComponent } from './pokemon-figure.component';

describe('PokemonFigureComponent', () => {
  let component: PokemonFigureComponent;
  let fixture: ComponentFixture<PokemonFigureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonFigureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokemonFigureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
