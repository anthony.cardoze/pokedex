import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-figure',
  templateUrl: './pokemon-figure.component.html',
  styleUrls: ['./pokemon-figure.component.css'],
})
export class PokemonFigureComponent {
  @Input() imageSrc: string = '../../../assets/images/006.png';
  @Input() imageAlt: string = 'charizard image';
}
