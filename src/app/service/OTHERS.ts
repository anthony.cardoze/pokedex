const OTHERS = [
  {
    imageSrc: '../../../assets/images/sqBlack.png',
    imageAlt: 'Charizard image',
  },
  {
    imageSrc: '../../../assets/images/Mewtwo.webp',
    imageAlt: 'Charizard image',
  },
  {
    imageSrc: '../../../assets/images/Solgaleo.webp',
    imageAlt: 'Charizard image',
  },
  {
    imageSrc: '../../../assets/images/snorlax.png',
    imageAlt: 'Charizard image',
  },
];

export default OTHERS;
